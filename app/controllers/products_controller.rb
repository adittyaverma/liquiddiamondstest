require 'csv'
class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  # GET /products
  # GET /products.json
  def index
    @products = Product.where(Product.search_product(params)).paginate(page: params[:page], per_page: 25)
  end

  def export_excel
    if params[:data].present?
      data = JSON params[:data]
      @csv_data = CSV.generate do |csv|
        csv << data.first.keys
        data.each do |t|
          arr = t.values
          arr[0] = '=HYPERLINK("'"https://liquiddiamonds.herokuapp.com/products/#{arr[0]}"'", "'"#{arr[0]}"'")'

            # '=HYPERLINK("http://stackoverflow.com", "so.com")'
          csv << arr
        end
      end
      send_data @csv_data, :type => 'text/xls; charset=iso-8859-1; header=present', :disposition => "attachment; filename=products.xls"
      flash[:notice] = 'File Downloaded'
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def product_params
      params.require(:product).permit(:name, :model, :brand, :year, :ram, :external_storage, :price)
    end
end

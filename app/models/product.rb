class Product < ApplicationRecord

	def self.search_product(params)
		query = "id is not null"
    query = query + " and name like '%#{params[:name]}%'" unless params[:name].blank?
    query = query + " and model like '%#{params[:model]}%'" unless params[:model].blank?
    query = query + " and brand like '%#{params[:brand]}%'" unless params[:brand].blank?
    query = query + " and ram like '%#{params[:ram]}%'" unless params[:ram].blank?
    query = query + " and external_storage like '%#{params[:external_storage]}%'" unless params[:external_storage].blank?
    return query
	end

end

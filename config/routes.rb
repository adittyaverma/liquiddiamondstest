Rails.application.routes.draw do
  resources :products
  devise_for :users

  # devise_for :users, path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'signup' }

  post "/export-excel" => "products#export_excel"

  devise_scope :user do
    root 'devise/sessions#new' 
  end

end

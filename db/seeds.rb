# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
500.times do
	product = Product.new
	product.name = Faker::Device.model_name
	product.model = Faker::Device.model_name
	product.brand = Faker::Device.manufacturer
	product.year = Faker::Number.between(from: 2016, to: 2020)
	product.ram = ["1GB","2GB","3GB","4GB","6GB","8GB","10GB"].sample
	product.external_storage = ["2GB","4GB","8GB","16GB","32GB","64GB","128GB"].sample
	product.price = Faker::Number.number(digits: 5)
	product.save!
end
